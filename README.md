#  ***ManianguaVPN*** - ***Simple python VPN Connection*** ✔️ | 
[![Python 2.x](https://img.shields.io/badge/python-2.x-blue.svg)]()
[![Stage](https://img.shields.io/badge/Release-Stable-brightgreen.svg)]()
[![Build](https://img.shields.io/badge/Supported_OS-Ubuntu,Kali,Arch,Parrot-blue,Android.svg)]()
[![Awesome](https://awesome.re/badge.svg)](https://awesome.re)
 
# ***Description***: 

Simple script python To make a connection VPN 

## ***Installation Linux*** :
```shell
$ git clone https://github.com/Sanix-Darker/ManianguaVPN

$ cd ManianguaVPN 

$ python2 Maniangua.py
```
## ***Installation Windows*** :
Still coding the bash way 

## ***Screenshot*** :
<img src="Capture1.PNG" width="70%"></img>
